const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let body = req.body;
    if(body.health === undefined){
        body.health = 100;
    }
    let isFieldsValid = fieldCreateFighterValidation(body);
    if (isFieldsValid.error){
        res.status(400).send(isFieldsValid);
        return;
    }
    let isPowerValid = validatePower(body.power);
    if (isPowerValid.error){
        res.status(400).send(isPowerValid);
        return;
    }
    let isDefenseValid = validateDefense(body.defense);
    if (isDefenseValid.error){
        res.status(400).send(isDefenseValid);
        return;
    }
    let isHealthValid = validateHealth(body.health);
    if (isHealthValid.error){
        res.status(400).send(isHealthValid);
        return;
    }
    next();
}

function fieldCreateFighterValidation(body){
    if(body['id'] !== undefined){
        return {'error': true, 'message': "Body can't have 'id' field"}
    }
    let fields = [];
    let fieldsWrongDT = [];
    for(let ch in fighter){
        if(body[ch] === undefined && ch !== 'id' && ch != 'health'){
            fields.push(ch);
        }
        if(typeof(body[ch]) !== typeof(fighter[ch]) && ch !== 'id' && ch != 'health'){
            fieldsWrongDT.push(ch);
        }
    }
    if(fields.length != 0){
        return {'error': true, 'message': `Fields required: ${fields}`};
    } 
    if(fieldsWrongDT.length !== 0){
        return {'error': true, 'message': `Found fields with wrong data type: ${fieldsWrongDT}`};
    }
    let validFields = Object.keys(fighter);
    validFields.splice(validFields.indexOf('id'), 1);

    let excessFields = [];
    for(let ch in body){
        if(validFields.indexOf(ch) === -1){
            excessFields.push(ch);
        }
    }
    if(excessFields.length !== 0){
        return {'error': true, 'message': `Found unnecessary fields: ${excessFields}`};
    }
    return {'error': false};   
}

function validatePower(power){
    if(typeof(power) !== 'number'){
        return {'error': true, 'message': 'Invalid power value'};
    }
    if(power < 1 || power > 100){
        return {'error': true, 'message': 'Invalid power value'};
    }
    return {'error': false};  
}

function validateDefense(defense){
    if(typeof(defense) !== 'number'){
        return {'error': true, 'message': 'Invalid defense value'};
    }
    if(defense < 1 || defense > 10){
        return {'error': true, 'message': 'Invalid defense value'};
    }
    return {'error': false};  
}

function validateHealth(health){
    if(typeof(health) != 'number'){
        return {'error': true, 'message': 'Invalid health value'};
    }
    if(health < 80 || health > 120){
        return {'error': true, 'message': 'Invalid health value'};
    }
    return {'error': false};  
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;