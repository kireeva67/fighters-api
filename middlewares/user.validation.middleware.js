const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let body = req.body;
    let isFieldsValid = fieldCreateUserValidation(body);
    if (isFieldsValid.error){
        res.status(400).send(isFieldsValid);
        return;
    }
    let isEmailValid = validateMail(body.email);
    if (isEmailValid.error){
        res.status(400).send(isEmailValid);
        return;
    }
    let isPhoneNumberValid = validatePhoneNumber(body.phoneNumber);
    if (isPhoneNumberValid.error){
        res.status(400).send(isPhoneNumberValid);
        return;
    }
    let isPasswordValid = validatePassword(body.password);
    if (isPasswordValid.error){
        res.status(400).send(isPasswordValid);
        return;
    }
    next();
}

function fieldCreateUserValidation(body){
    if(body['id'] !== undefined){
        return {'error': true, 'message': "Body can't have 'id' field"}
    }
    let fields = [];
    let fieldsWrongDT = [];
    for(let ch in user){
        if(body[ch] === undefined && ch !== 'id'){
            fields.push(ch);
        }
        if(typeof(body[ch]) !== typeof(user[ch]) && ch !== 'id'){
            fieldsWrongDT.push(ch);
        }
    }
    if(fields.length != 0){
        return {'error': true, 'message': `Fields required: ${fields}`};
    } 
    if(fieldsWrongDT.length !== 0){
        return {'error': true, 'message': `Found fields with wrong data type: ${fieldsWrongDT}`};
    }
    let validFields = Object.keys(user);
    validFields.splice(validFields.indexOf('id'), 1);

    let excessFields = [];
    for(let ch in body){
        if(validFields.indexOf(ch) === -1){
            excessFields.push(ch);
        }
    }
    if(excessFields.length !== 0){
        return {'error': true, 'message': `Found unnecessary fields: ${excessFields}`};
    }
    return {'error': false};   
}

function validateMail(email){
    if(!email.endsWith('@gmail.com')){
        return {'error': true, 'message': "Invalid email"};
    }
    if(email.split('@')[0].length < 2){
        return {'error': true, 'message': 'Invalid email'};
    }
    return {'error': false};
}

function validatePhoneNumber(phoneNumber){
    if(!phoneNumber.startsWith('+380')){
        return {'error': true, 'message': 'Invalid phone number'};
    }
    if(phoneNumber.length !== 13){
        return {'error': true, 'message': 'Invalid phone number'};
    }
    return {'error': false};
}

function validatePassword(password){
    if(typeof(password) !== 'string'){
        return {'error': true, 'message': 'Invalid password'};
    }
    if(password.length <= 3){
        return {'error': true, 'message': 'Invalid password'};
    }
    return {'error': false};
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;