const { BaseRepository } = require('./baseRepository');
const fs = require('fs');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    readFileData(){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData);
        return db.fighters;
    }

    getOne(search){
        let fighters = this.readFileData();
        for(let ch = 0; ch < fighters.length; ch++){
           if (fighters[ch]['id'] === search['id']){
               return fighters[ch];
           }
        }
        return false;
    }

    addFighter(fighter){
        let fighters = this.readFileData();
        let isDuplications = this.checkFieldDuplications(fighters, fighter);
        if(isDuplications.error){
            return isDuplications;
        }
        let id = this.newID(fighters);
        fighter['id'] = id;
        this.addFighterToDB(fighter);
        return {'error': false};
    }

    checkFieldDuplications(fighters, fighter){
        for(let ch = 0; ch < fighters.length; ch++){
            if(fighters[ch]['name'].toLowerCase() === fighter['name'].toLowerCase()){
                return {'error': true, 'message': 'Name already exist'};
            }
        }
        return {'error': false};
    }

    newID(fighters){
        let maxId = 0;
        for(let ch = 0; ch < fighters.length; ch++){
            if(Number(fighters[ch]['id']) > Number(maxId)){
                maxId = fighters[ch]['id'];
            }
        }
        return String(Number(maxId) + 1);
    }

    addFighterToDB(fighter){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData); 
        db.fighters.push(fighter);
        let newDB = JSON.stringify(db, null, 2);
        fs.writeFileSync('database.json', newDB);
    }

    deleteFighter(deleteID){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData); 
        let fighters = db.fighters;
        for(let ch = 0; ch < fighters.length; ch++){
            if(fighters[ch]['id'] === deleteID['id']){
                fighters.splice(ch, 1);
                break;
            }
        }
        let newDB = JSON.stringify(db, null, 2);
        fs.writeFileSync('database.json', newDB);
        return {'error': false};
    }
}

exports.FighterRepository = new FighterRepository();