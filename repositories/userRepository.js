const { BaseRepository } = require('./baseRepository');
const fs = require('fs');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    readFileData(){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData);
        return db.users;
    }

    getOne(search){
        let users = this.readFileData();
        for(let ch = 0; ch < users.length; ch++){
           if (users[ch]['id'] === search['id']){
               return users[ch];
           }
        }
        return false;
    }

    addUser(user){
        let users = this.readFileData();
        let isDuplications = this.checkFieldDuplications(users, user);
        if(isDuplications.error){
            return isDuplications;
        }
        let id = this.newId(users);
        user['id'] = id; 
        this.addUserToDB(user);
        return {'error': false};
    }

    checkFieldDuplications(users, user){
        for(let ch = 0; ch < users.length; ch++){
            if(users[ch]['email'].toLowerCase() === user['email'].toLowerCase()){
                return {'error': true, 'message': 'Email already exist'};
            }
            if(users[ch]['phoneNumber'] === user['phoneNumber']){
                return {'error': true, 'message': 'Phone number already exist'};
            }
        }
        return {'error': false};
    }

    newId (users){
        let maxId = 0;
        for(let ch = 0; ch < users.length; ch++){
            if(Number(users[ch]['id']) > Number(maxId)){
                maxId = users[ch]['id'];
            }
        }
        return String(Number(maxId) + 1);
    }

    addUserToDB(user){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData); 
        db.users.push(user);
        let newDB = JSON.stringify(db, null, 2);
        fs.writeFileSync('database.json', newDB);
    }

    deleteUser(deleteID){
        let rawData = fs.readFileSync('database.json');
        let db = JSON.parse(rawData); 
        let users = db.users;
        for(let ch = 0; ch < users.length; ch++){
            if(users[ch]['id'] === deleteID['id']){
                users.splice(ch, 1);
                break;
            }
        }
        let newDB = JSON.stringify(db, null, 2);
        fs.writeFileSync('database.json', newDB);
        return {'error': false};
    }
}

exports.UserRepository = new UserRepository();