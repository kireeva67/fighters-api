const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const fighterService = require('../services/fighterService');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res) {
    let fighters = fighterService.allFighters();
    res.send(fighters);
});

router.get('/:id', function(req, res) {
    let search = req.params;
    let searchResult = FighterService.search(search);
    if(searchResult === null){
        res.status(404).send({'error': true, 'message': 'Fighter not found'});
    }
    else{
        res.send(searchResult);
    }
});

router.post('/', function(req, res, next) {
    createFighterValid(req, res, next);
});

router.post('/', function(req, res) {
    let fighter = req.body;
    let status = FighterService.addFighter(fighter);
    if(status.error){
        res.status(400).send(status);
    }
    else{
        res.send({'error': false, 'message': status.message});
    }
})

router.delete('/:id', function(req, res) {
    let deleteID = req.params;
    let status = FighterService.deleteFighter(deleteID);
    if(status.error){
        res.status(400).send(status);
    }
    else{
        res.send({'error': false, 'message': status.message});
    }
})

module.exports = router;