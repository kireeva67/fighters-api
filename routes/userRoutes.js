const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get ('/', function(req, res) {
    let users = UserService.allUsers();
    res.send(users);
});

router.get('/:id', function(req, res) {
    let search = req.params;
    let searchResult = UserService.search(search);
    if(searchResult === null){
        res.status(404).send({'error': true, 'message': 'User not found'});
    }
    else{
        res.send(searchResult);
    }
});

router.post('/', function(req, res, next) {
    createUserValid(req, res, next);
});

router.post('/', function(req, res) {
    let user = req.body;
    let status = UserService.addUser(user);
    if(status.error){
        res.status(400).send(status);
    }
    else{
        res.send({'error': false, 'message': status.message});
    }
})

router.delete('/:id', function(req, res) {
    let deleteID = req.params;
    let status = UserService.deleteUser(deleteID);
    if(status.error){
        res.status(400).send(status);
    }
    else{
        res.send({'error': false, 'message': status.message});
    }
})

module.exports = router;