const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    allFighters(){
        return FighterRepository.readFileData();
    }

    addFighter(fighter) {
        let status = FighterRepository.addFighter(fighter);
        if(status.error){
            return status;
        }
        return {'error': false, 'message': 'Fighter added'}
    }

    deleteFighter(deleteID){
        let status = FighterRepository.deleteFighter(deleteID);
        if(status.error){
            return status;
        }
        return {'error': false, 'message': 'Fighter deleted'}
    }
}

module.exports = new FighterService();