const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    allUsers(){
        return UserRepository.readFileData();
    }

    addUser(user){
        let status = UserRepository.addUser(user);
        if(status.error){
            return status;
        }
        return {'error': false, 'message': 'User added'}
    }

    deleteUser(deleteID){
        let status = UserRepository.deleteUser(deleteID);
        if(status.error){
            return status;
        }
        return {'error': false, 'message': 'User deleted'}
    }
}

module.exports = new UserService();